package com.example.myapplication

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView

import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), View.OnClickListener {
    internal var mNumText: TextView? = null
    internal var mStartButton: Button? = null

    /**
     * Broadcast receiver to receive the data
     */
    private val mReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            val num = intent.getLongExtra("int", 0)
            Log.d("num", num.toString())
            mNumText!!.text = num.toString()
        }
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

                mNumText = dateText
                mStartButton = startButton

        mStartButton!!.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        // unregister local broadcast
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver)
    }

    override fun onResume() {
        super.onResume()

        // register local broadcast
                 var filter:IntentFilter =  IntentFilter(MyIntentService.CUSTOM_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, filter)
    }

    override fun onClick(view: View) {
//                if (view.getId() == R.id.startButton) {
                    // start intent service
                    Log.d("clicked", "clicked")
                    var intent = Intent(this, MyIntentService::class.java)
                    startService(intent)
//                }
    }
}

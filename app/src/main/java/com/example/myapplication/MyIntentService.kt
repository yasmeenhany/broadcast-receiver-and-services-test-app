package com.example.myapplication

import android.annotation.SuppressLint
import android.app.IntentService
import android.content.Intent
import android.util.Log

import androidx.localbroadcastmanager.content.LocalBroadcastManager

import java.util.Date
import io.reactivex.Observable
import java.util.concurrent.TimeUnit


class MyIntentService : IntentService("MyIntentService") {

    @SuppressLint("CheckResult")
    override fun onHandleIntent(arg0: Intent?) {
        Log.d("service status", "started")
        Observable.interval(0, 3, TimeUnit.SECONDS )
            .subscribe { value ->
                Log.d("value", value.toString())
                val intent = Intent(CUSTOM_ACTION)
                intent.putExtra("int", value)
                Log.d(MyIntentService::class.java.simpleName, "sending broadcast")

                // send local broadcast
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
            }

    }

    companion object {

        val CUSTOM_ACTION = "YOUR_CUSTOM_ACTION"
    }

}